# Wink - Eyes gesture Unity plugin

Wink is a plugin, that allows to detect eye gestures. By default it's able to recognise amusement, wonderment, anger and boredom (in context of talk with NPC), but user can define own ones easily.

## Pre-requisites:
1. Download newest [HMD Eyes plugin from Pupil Labs' official github](https://github.com/pupil-labs/hmd-eyes)
2. Download newest [Pupil Labs environment from official github](https://github.com/pupil-labs/pupil/releases)
3. Get latest [Wink plugin](https://gitlab.com/Loonachip96/wink.git)
4. Get Unity Game Engine 2017.4.21 [from here](https://unity3d.com/get-unity/download/archive)
	
	
## Setting up Unity Project:
1. Import SteamVR plugin into project, using Unity Asset Store, and run scene to trigger SteamVR configuration
2. Copy folder ``` <hmd-eyes-directory>\unity_pupil_plugin_vr\Assets\pupil_plugin ``` to your project's Assets directory. It will be automatically loaded into project.
3. Add "[WinkCamera]" prefab (Wink_plugin\Prefabs) and attach Steam's "[CameraRig]" prefab and Pupil labs' "Pupil Manager" prefab to it
4. Run Pupil Capture / Pupil Service (from prerequisites 2.)
5. Attach Wink Interpreter Script to any object interested in gesture occurence

## Defining own Wink Interpreter Script
1. Create script, which will inherit after Wink::WinkGestureInterpreter
2. Implement OnBufferFilled and OnSampleFilled methods
3. Operate inside them to decide if gesture has occured.
Examples are available under Wink_plugin/Scripts/WinkInterpreterScripts

## Researching characteristics of a custom gesture
'''
Incoming
'''