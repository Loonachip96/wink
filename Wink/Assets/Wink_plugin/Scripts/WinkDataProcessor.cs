﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEditor;

public class ReadOnlyAttribute : PropertyAttribute {}

namespace Wink
{
    public class WinkDataProcessor : WinkInterface
    {
        #region Delegates
        public delegate void OnBufferFilledDelegate(ReadOnlyCollection<WinkDataEntity> sampleBuffer);
        public delegate void OnSampleCapturedDelegate(WinkDataEntity sample);

        public static event OnBufferFilledDelegate OnBufferFilled;
        public static event OnSampleCapturedDelegate OnSampleCaptured;
        #endregion
        
        #region Declarations
        [ReadOnlyAttribute] public WinkDataEntity sample;

        public double bufferingTimeInSeconds;
        private int bufferIterator;
        private List<List<WinkDataEntity>> buffers;

        private double timeCounter = 0;
        private int sampleCounter = 0;
        private WinkManager parent;
        private GameObject gazeRayObject;
        private GameObject faceRayObject;
        private Vector3 standardViewportPoint;
        private Camera sceneCamera = null;
        #endregion


        void Start()
        {
            standardViewportPoint = new Vector3(0.5f, 0.5f, 10);

            if (PupilTools.IsConnected)
            {
                PupilTools.IsGazing = true;
                PupilTools.SubscribeTo("gaze");
            }
            parent = this.transform.parent.GetComponentInParent<WinkManager>();

            bufferIterator = 0;
            buffers = new List<List<WinkDataEntity>>();
            for (int i = 0; i < parent.multiBufferingCount; i++)
                buffers.Add(new List<WinkDataEntity>());

            bufferingTimeInSeconds = parent.bufferingTimeInSeconds;
            
            faceRayObject = new GameObject("WinkFaceRay");
            faceRayObject.AddComponent<LineRenderer>();
            faceRayObject.transform.SetParent(this.transform);
            faceRayObject.GetComponent<LineRenderer>().enabled = false;
            faceRayObject.GetComponent<LineRenderer>().startWidth = parent.rayWidth;
            faceRayObject.GetComponent<LineRenderer>().endWidth = parent.rayWidth;

            gazeRayObject = new GameObject("WinkGazeRay");
            gazeRayObject.AddComponent<LineRenderer>();
            gazeRayObject.transform.SetParent(this.transform);
            gazeRayObject.GetComponent<LineRenderer>().enabled = false;
            gazeRayObject.GetComponent<LineRenderer>().startWidth = parent.rayWidth;
            gazeRayObject.GetComponent<LineRenderer>().endWidth = parent.rayWidth;
        }

        void Update()
        {
            findPlayerCamera();

            if (PupilTools.IsConnected && parent.calibrationDone && sceneCamera != null)
            {
                faceRayObject.GetComponent<LineRenderer>().enabled = parent.showFaceRay;
                gazeRayObject.GetComponent<LineRenderer>().enabled = parent.showGazeRay;

                fillSample();

                timeCounter += Time.deltaTime;
                sampleCounter++;

                if (timeCounter > bufferingTimeInSeconds)
                {
                    runOnBufferFilled();
                    swapBuffers();
                }

                if (OnSampleCaptured != null)
                    OnSampleCaptured(sample);

                buffers[bufferIterator].Add(sample);
            }
        }


        #region DataSharing
        void swapBuffers()
        {
            int oldBufferIndex = bufferIterator;

            bufferIterator = (bufferIterator + 1) % parent.multiBufferingCount;
            buffers[oldBufferIndex].Clear();

            timeCounter = 0;
            sampleCounter = 0;
        }

        void runOnBufferFilled()
        {
            if (OnBufferFilled != null)
            {
                OnBufferFilled(buffers[bufferIterator].AsReadOnly());
            }
        }
        #endregion

        #region DataCollection
        void findPlayerCamera()
        {
            if (sceneCamera == null)
            {
                try
                {
                    sceneCamera = parent.rigObject.GetComponentInChildren<Camera>();
                    if (sceneCamera == null)
                        sceneCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
                }
                catch
                {
                    Debug.Log("[Wink] Cannot find Camera Rig object called " + parent.VRRigObjectName + ". Check if it's correct in WinkManager.");
                }
            }
        }

        void fillSample()
        {
            sample.deltaTime = Time.deltaTime;

            sample.avgGazingUncertainty = new Vector2(
                    (
                        Mathf.Abs(PupilData._2D.LeftEyePosition.x - PupilData._2D.GazePosition.x)
                        +
                        Mathf.Abs(PupilData._2D.RightEyePosition.x - PupilData._2D.GazePosition.x)
                    ) / 2
                ,
                    (
                        Mathf.Abs(PupilData._2D.LeftEyePosition.y - PupilData._2D.GazePosition.y)
                        +
                        Mathf.Abs(PupilData._2D.RightEyePosition.y - PupilData._2D.GazePosition.y)
                    ) / 2
                );


            if(sampleCounter > 0)
            {
                WinkDataEntity previousSample = getCurrentBuffer()[sampleCounter - 1];
                Vector2 newPoint = new Vector2(PupilData._2D.GazePosition.x, PupilData._2D.GazePosition.y);
                Vector2 prevPoint = new Vector2(previousSample.gazePosition.x, previousSample.gazePosition.y);
                float distance = distanceBetween(newPoint, prevPoint);

                if (distance > parent.gazeSpeed)
                {
                    sample.gazePosition = new Vector2(
                        Mathf.Sign(prevPoint.x - newPoint.x) * parent.gazeSpeed,
                        Mathf.Sign(prevPoint.y - newPoint.y) * parent.gazeSpeed
                    );

                    /*float directionAngle = Mathf.Atan2(newPoint.y, newPoint.x);
                    sample.gazePosition = new Vector2(
                        parent.gazeSpeed * distance * Mathf.Sin(directionAngle),
                        parent.gazeSpeed * distance * Mathf.Cos(directionAngle)
                    );
                    sample.gazePosition = new Vector2(
                        prevPoint.x + parent.gazeSpeed * Mathf.Sin(directionAngle),
                        prevPoint.y + parent.gazeSpeed * Mathf.Cos(directionAngle)
                    );*/
                }
                else
                    sample.gazePosition = newPoint;
            }
            else
                sample.gazePosition = PupilData._2D.GazePosition;

            sample.gazePosition = clip0to1(sample.gazePosition);
            sample.headPosition = sceneCamera.transform.position;
            sample.headRotationQuaternion = sceneCamera.transform.rotation;
            sample.headRotationAroundAxis = fixAnglesToWorldAxies(sceneCamera.transform.eulerAngles);
            sample.gazeRaycastHitObjectName = getHitnameFromGazeRaycast();
            sample.faceRaycastHitObjectName = getHitnameFromFaceRaycast();


            if (sampleCounter > 0)
            {
                int samplesAmount = buffers[bufferIterator].Count;
                WinkDataEntity previousSample = buffers[bufferIterator][samplesAmount - 1];

                sample.gazeJumpDistance = Vector2.Distance(previousSample.gazePosition, sample.gazePosition);
                sample.gazeVelocity = sample.gazeJumpDistance / Time.deltaTime;
            }
            else
            {
                sample.gazeJumpDistance = 0;
                sample.gazeVelocity = 0;
            }
        }

        string getHitnameFromGazeRaycast()
        {
            LineRenderer rayRenderer = gazeRayObject.GetComponent<LineRenderer>();
            Ray heading;
            RaycastHit raycastHit;
            Vector3 viewportPoint = standardViewportPoint;
            List<WinkDataEntity> buffer = buffers[bufferIterator];
            WinkDataEntity lastSample;


            if (PupilTools.IsConnected && PupilTools.IsGazing && sceneCamera != null && buffer.Count > 0)
            {
                lastSample = buffer[buffer.Count - 1];
                viewportPoint = new Vector3(lastSample.gazePosition.x, lastSample.gazePosition.y, 1f);
            }

            heading = sceneCamera.ViewportPointToRay(viewportPoint);

            if (Physics.Raycast(heading, out raycastHit))
                rayRenderer.SetPosition(1, raycastHit.point);
            else
                rayRenderer.SetPosition(1, heading.origin + heading.direction * parent.rayRange);

            if (rayRenderer != null)
                rayRenderer.SetPosition(0, sceneCamera.transform.position - new Vector3(0, 0.1f, 0));

            if (raycastHit.collider != null)
                return raycastHit.collider.name;
            else
                return string.Empty;
        }

        string getHitnameFromFaceRaycast()
        {
            LineRenderer rayRenderer = faceRayObject.GetComponent<LineRenderer>();
            Ray heading;
            RaycastHit raycastHit;
            Vector3 viewportPoint = standardViewportPoint;

            heading = sceneCamera.ViewportPointToRay(viewportPoint);

            if (Physics.Raycast(heading, out raycastHit))
                rayRenderer.SetPosition(1, raycastHit.point);
            else
                rayRenderer.SetPosition(1, heading.origin + heading.direction * parent.rayRange);

            if (rayRenderer != null)
                rayRenderer.SetPosition(0, sceneCamera.transform.position - new Vector3(0, 0.1f, 0));

            if (raycastHit.collider != null)
                return raycastHit.collider.name;
            else
                return string.Empty;
        }
        #endregion
        
        private List<WinkDataEntity> getCurrentBuffer()
        {
            return buffers[bufferIterator];
        }

        private float distanceBetween(Vector2 p1, Vector2 p2)
        {
            return Mathf.Sqrt(Mathf.Pow(p1.x - p2.x, 2) + Mathf.Pow(p1.y - p2.y, 2));
        }
    }
}

#region ForUnityInspector
[CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
public class ReadOnlyAttributeDrawer : PropertyDrawer
{
    // Necessary since some properties tend to collapse smaller than their content
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return EditorGUI.GetPropertyHeight(property, label, true);
    }

    // Draw a disabled property field
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        //GUI.enabled = !Application.isPlaying;
        GUI.enabled = false;
        EditorGUI.PropertyField(position, property, label, true);
        GUI.enabled = true;
    }
}
#endregion

