﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Wink;

public class AngerWinkInterpreter : WinkGestureInterpreter {

    protected override void OnBufferFilled()
    {
        string thisColliderName     = colliderName;
        headOffsetOfThisObject.y    = 1f;

        ////////////////////// REQUIREMENTS TO OCCUR //////////////////////
        double minimalPercentageTimeFacingOnThisObject  = 99;
        double minimalPercentageTimeGazingOnThisObject  = 99;
        double minimalAverageAngleHeadTiltToLeft        = 10;
        double minimalFaceFixation                      = 0.9;
        //double maximalGazeFixation                      = 0.92; // this depends on object size
        double minimalStdDevOfGazeVelocity              = 0.45; // this depends on object size
        ///////////////////////////////////////////////////////////////////

        double percentageTimeFacingOnThisObject;
        double percentageTimeGazingOnThisObject;
        double averageheadTilt;
        double faceFixation;
        double gazeFixation;
        double stdDevOfGazeVelocity;


        // collect needed data
        averageheadTilt                     = getStatisticsOfHeadRotationAroundAxisZ().avg;
        faceFixation                        = getHeadFixation();
        gazeFixation                        = getGazeFixation();
        percentageTimeGazingOnThisObject    = getPercentageTimeGazingObjectWithName(thisColliderName);
        percentageTimeFacingOnThisObject    = getPercentageTimeFacingObjectWithName(thisColliderName);
        stdDevOfGazeVelocity = getStatisticsOfGazeVelocity().stdDev;

        // make decisions
        bool isHighlyFacingThisObject       = percentageTimeFacingOnThisObject >= minimalPercentageTimeFacingOnThisObject;
        bool isHighlyGazingThisObject       = percentageTimeGazingOnThisObject >= minimalPercentageTimeGazingOnThisObject;
        bool isTiltingHeadToLeft            = averageheadTilt >= minimalAverageAngleHeadTiltToLeft;
        //bool hasLowGazeFixation             = gazeFixation < maximalGazeFixation;
        bool hasHighFaceFixation            = faceFixation >= minimalFaceFixation;
        bool hasRunningEyes                 = stdDevOfGazeVelocity >= minimalStdDevOfGazeVelocity;


        if (
            isHighlyFacingThisObject
            && isHighlyGazingThisObject
            && isTiltingHeadToLeft
            //&& hasLowGazeFixation
            && hasHighFaceFixation
            && hasRunningEyes
        )
        {
            Debug.Log("Anger detected!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            this.transform.Translate(new Vector3(0, 15f, 0));
        }
    }

    protected override void OnSampleFilled(WinkDataEntity sample)
    {
    }
}
