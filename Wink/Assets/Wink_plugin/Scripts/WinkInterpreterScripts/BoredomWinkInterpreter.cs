﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Wink;

public class BoredomWinkInterpreter : WinkGestureInterpreter
{

    protected override void OnBufferFilled()
    {
        string thisColliderName     = colliderName;
        headOffsetOfThisObject.y    = 1f;

        ////////////////////// REQUIREMENTS TO OCCUR //////////////////////
        double minimalPercentageTimeFacingOnThisObject  = 99;
        double maximalPercentageTimeGazingOnThisObject  = 10;
        double minimalFaceFixation                      = 0.9;
        ///////////////////////////////////////////////////////////////////

        double percentageTimeFacingOnThisObject;
        double percentageTimeGazingOnThisObject;
        double faceFixation;


        // collect needed data
        percentageTimeGazingOnThisObject    = getPercentageTimeGazingObjectWithName(thisColliderName);
        percentageTimeFacingOnThisObject    = getPercentageTimeFacingObjectWithName(thisColliderName);
        faceFixation                        = getHeadFixation();

        // make decisions
        bool isHighlyFacingThisObject       = percentageTimeFacingOnThisObject >= minimalPercentageTimeFacingOnThisObject;
        bool isRarelyLookingAtThisObject    = percentageTimeGazingOnThisObject < maximalPercentageTimeGazingOnThisObject;
        bool hasHighFaceFixation            = faceFixation > minimalFaceFixation;


        if (
            isHighlyFacingThisObject
            && isRarelyLookingAtThisObject
            && hasHighFaceFixation
        )
        {
            Debug.Log("Boredom detected!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            this.transform.Translate(new Vector3(0, 15f, 0));
        }
    }

    protected override void OnSampleFilled(WinkDataEntity sample)
    {
    }

}
