﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Wink;

public class PuzzlementWinkInterpreter : WinkGestureInterpreter
{
    protected override void OnBufferFilled()
    {
        string thisColliderName     = colliderName;
        headOffsetOfThisObject.y    = 1f;

        ////////////////////// REQUIREMENTS TO OCCUR //////////////////////
        double minimalPercentageTimeFacingOnThisObject  = 99;
        double minimalPercentageTimeGazingOnThisObject  = 99;
        double minimalAverageAngleHeadTiltToLeft        = 10;
        double minimalFaceFixation                      = 0.9;
        double minimalGazeFixation                      = 0.93; 
        ///////////////////////////////////////////////////////////////////

        double percentageTimeFacingOnThisObject;
        double percentageTimeGazingOnThisObject;
        double averageheadTilt;
        double faceFixation;
        double gazeFixation;


        // collect needed data
        averageheadTilt                     = getStatisticsOfHeadRotationAroundAxisZ().avg;
        faceFixation                        = getHeadFixation();
        gazeFixation                        = getGazeFixation();
        percentageTimeGazingOnThisObject    = getPercentageTimeGazingObjectWithName(thisColliderName);
        percentageTimeFacingOnThisObject    = getPercentageTimeFacingObjectWithName(thisColliderName);

        // make decisions
        bool isHighlyFacingThisObject   = percentageTimeFacingOnThisObject >= minimalPercentageTimeFacingOnThisObject;
        bool isHighlyGazingThisObject   = percentageTimeGazingOnThisObject >= minimalPercentageTimeGazingOnThisObject;
        bool isTiltingHeadToLeft        = averageheadTilt >= minimalAverageAngleHeadTiltToLeft;
        bool hasHighGazeFixation        = gazeFixation >= minimalGazeFixation;
        bool hasHighFaceFixation        = faceFixation >= minimalFaceFixation;


        if (
            isHighlyFacingThisObject 
            && isHighlyGazingThisObject 
            && isTiltingHeadToLeft
            && hasHighGazeFixation
            && hasHighFaceFixation
        )
        {
            Debug.Log("Puzzlement detected!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            this.transform.Translate(new Vector3(0, 15f, 0));
        }
    }

    protected override void OnSampleFilled(WinkDataEntity sample)
    {
    }
}
