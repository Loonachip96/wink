﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Wink
{
    public class WinkManager : MonoBehaviour
    {
        public string VRRigObjectName = "[CameraRig]";
        public GameObject rigObject;
        public List<GameObject> gameObjectsToEnableAfterCalibration;
		public KeyCode recordKey = KeyCode.Backspace;
		public int multiBufferingCount = 2;
        public double bufferingTimeInSeconds = 3;
        public bool calibrationDone = false;
        public bool showFaceRay = false;
        public bool showGazeRay = false;
        public float rayRange = 100f;
        public float rayWidth = 0.1f;
        public float gazeSpeed = 0.07f;

        void Start()
        {
            rigObject = GameObject.Find(VRRigObjectName);
            if (rigObject)
                rigObject.SetActive(false);

            PupilTools.OnCalibrationStarted += OnCalibtaionStarted;
            PupilTools.OnCalibrationEnded += OnCalibrationEnded;

			if (multiBufferingCount < 1) multiBufferingCount = 1;

            if (gazeSpeed < 0) gazeSpeed = 0;
            if (gazeSpeed > 1) gazeSpeed = 1;
        }

        void OnCalibtaionStarted()
        {
			if (rigObject)
				rigObject.SetActive (false);

            foreach (GameObject go in gameObjectsToEnableAfterCalibration)
            {
                go.SetActive(false);
            }
        }

        void OnCalibrationEnded()
		{
			if (rigObject)
				rigObject.SetActive (true);

            calibrationDone = true;

            foreach (GameObject go in gameObjectsToEnableAfterCalibration)
            {
                go.SetActive(true);
            }

            GameObject recorder;
            recorder = new GameObject("Wink Recorder");
            recorder.transform.SetParent(this.transform);
            recorder.AddComponent<WinkRecorder>();

            GameObject processor;
            processor = new GameObject("Wink Data Processor");
            processor.transform.SetParent(this.transform);
            processor.AddComponent<WinkDataProcessor>();
        }
    }
}
