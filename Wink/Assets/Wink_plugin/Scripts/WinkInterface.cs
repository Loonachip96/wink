﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Wink
{
	public class WinkInterface : MonoBehaviour
	{
		public enum VarName
		{
			deltaTime,
			calibrationConfidence,
			avgGazingUncertainty,
			gazePosition,
			gazeRaycastHitObjectName,
			faceRaycastHitObjectName,
			headPosition,
			pheadRotationQuaternion,
			headRotationAroundAxis,
			gazeJumpDistance,
			gazeVelocity
		}

		public struct StatisticVector<T>
		{
			public T min { get; set; }
			public T max { get; set; }
			public T avg { get; set; }
			public T stdDev { get; set; }

			public StatisticVector(T everyValue)
			{
				this.min = everyValue;
				this.max = everyValue;
				this.avg = everyValue;
				this.stdDev = everyValue;
			}
			public StatisticVector(T min, T max, T avg, T stdDev)
			{
				this.min = min;
				this.max = max;
				this.avg = avg;
				this.stdDev = stdDev;
			}
		}

		public class Occurance
		{
			public string name { get; set; }
			public int occurance { get; set; }

			public Occurance(string name, int occurance)
			{
				this.name = name;
				this.occurance = occurance;
			}
		}

		public struct WinkDataEntity
		{
			public double deltaTime { get; set; }
			public double calibrationConfidence { get; set; }
			public Vector2 avgGazingUncertainty { get; set; }
			public Vector2 gazePosition { get; set; }
			public string gazeRaycastHitObjectName { get; set; }
			public string faceRaycastHitObjectName { get; set; }
			public Vector3 headPosition { get; set; }
			public Quaternion headRotationQuaternion { get; set; }
			public Vector3 headRotationAroundAxis { get; set; }
			public double gazeJumpDistance { get; set; }
			public double gazeVelocity { get; set; }
        }

        public double clipAngle(double angle)
        {
            return angle % 360f;
        }

        public Vector3 clipAngle(Vector3 angles)
        {
            Vector3 result = new Vector3(
                (float)clipAngle(angles.x), 
                (float)clipAngle(angles.y), 
                (float)clipAngle(angles.z)
            );
            return result;
        }

        public float clip0to1(float value)
        {
            if (value < 0) return 0;
            if (value > 1) return 1;
            return value;
        }

        public Vector2 clip0to1(Vector2 values)
        {
            Vector2 result = values;

            if (result.x < 0) result.x = 0;
            if (result.x > 1) result.x = 1;

            if (result.y < 0) result.y = 0;
            if (result.y > 1) result.y = 1;

            return values;
        }

        public Vector3 fixAnglesToWorldAxies(Vector3 angles)
        {
            Vector3 result = new Vector3();
            Vector3 antiAngles = new Vector3(
                    Mathf.Abs(360f - angles.x),
                    Mathf.Abs(360f - angles.y),
                    Mathf.Abs(360f - angles.z)
                );

            result.x = (antiAngles.x < angles.x) ? -antiAngles.x : angles.x;
            result.y = (antiAngles.y < angles.y) ? -antiAngles.y : angles.y;
            result.z = (antiAngles.z < angles.z) ? -antiAngles.z : angles.z;

            return result;
        }
    }
}