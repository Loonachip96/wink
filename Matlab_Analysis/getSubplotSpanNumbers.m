function [gID] = getSubplotSpanNumbers(posX, posY, spanX, spanY, resolX)
    
    %startSubGridNumber = posX + (resolX * (posY-1));
    
    startColumnNumbers = (posY-1:posY+spanY-2).*resolX + posX;
    
    gID = [startColumnNumbers'];
    
    for i=1:spanX-1
        gID = [ gID startColumnNumbers(:)+i ];
    end
    
    sort(gID,'ascend');
    
    gID = gID';
end

