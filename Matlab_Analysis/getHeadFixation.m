function fix = getHeadFixation(qXs, qYs, qZs, qWs)
    
    q = quaternion([qWs qXs qYs qZs]);
    rotations = rotm2axang(quat2rotm(q));
    angle = rad2deg(rotations(:, 4));
    rotAroundX = rotations(:, 1) .* angle;
    rotAroundY = rotations(:, 2) .* angle;
    rotAroundZ = rotations(:, 3) .* angle;

    
    fix = 1 - mean([std(rotAroundZ); std(rotAroundX); std(rotAroundY)]) / 180;
end

