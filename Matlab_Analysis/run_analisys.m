%  |------------- done
%  |    |-------- in progress
%  |    |   |--- todo
% TODO: Visualise head position & rotation
% TODO: Visualise gaze fixation
% TODO: Visualise gazing pane (tracePlot)
% TODO: Visualise raycasts' hit objects' time
% TODO: Visualise gaze speed
% TODO: Visualise heatmaps
    
close all
clear all
clc

%%%% METADATA %%%%
    names = {   'Damian',   'Kacper',  'Kasia', 'Maks',     'Natalia',    'Ola'};
    surnames = {'Moscicki', 'Iwanski', 'Plust', 'Wrzesien', 'Scislowska', 'Piskor'};
    gestures = {'ZADUMA', 'ZDZIWIENIE', 'ZLOSC', 'ZNUDZENIE'};
    NPCsLocation = {
        [-0.07 0.35 2.19], 
        [7.97 0.59 5.24], 
        [5.3 1.29 5.12], 
        [0.57 0.35 1.36]
        };
    globalOffset = [0 0 0];
%%%%%%%%%%%%%%%%%%

%%%% OPTIONS %%%%
debugCase = false;
attemptsPerGesture = 3;
keepFiguresOpened = false;
saveResultsToFiles = true;
    savePath = 'Results/';
printStatisticAnalisys = true;
    clearConsoleBeforeAnalisys = true;
    maxShownResultsOfOccurenceBeforeHidingProcedure = 20;
%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%% SETTINGS %%%%%%%%%%%%%%%%%
%       gestures = {'ZADUMA', 'ZDZIWIENIE', 'ZLOSC', 'ZNUDZENIE'};
NPCNamesPrefixes = {'NPCNear', 'Companion', 'Bartender', 'NPCFar'};

statisticEntityFormat = {
%   {id, roundValuesTo, comparementLevels, title},
    {1,     5,  	[-10 10],       'Forw./backw. deflection'},
    {2,     5,  	[-10 10],       'Deflection to left/right'},
    {3,     5,  	[-10 10],       'Rotation (c.clockwise)'},
    {4,     5,  	[10],           'Absolute forw./backw. deflection'},
    {5,     5,  	[10],           'Absolute deflection to left/right'},
    {6,     5,  	[10],           'Absolute rotation'},
    {7,     1,  	[25 50 75],     'Percentage time gazing on NPC'},
    {8,     -1, 	[],             'Frames of all people spent gazing on NPC (1 means looking at)'},
    {9,     1,  	[25 50 75],     'Percentage time facing NPC'},
    {10,	-1,     [],             'Frames of all people spent facing NPC (1 means facing him)'},
    {11,	-1, 	[],             'Average gazing uncertainty'},
    {12,	-1, 	[1],            'Average gaze speed'},
    {13,	-1, 	[],             'Maximal gaze speed'},
    {14,	-1, 	[],             'Minimal gaze speed'},
    {15,	-1, 	[],             'Standard deviation of gaze speed'},
    {16,	-1, 	[],             'Gaze fixation'},
    {17,	-1, 	[],             'Head fixation'}
};
% if want to change IDs, change them also in resultFormat_createResult.m
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if debugCase == true
    names = {'Kacper', 'Damian'};
    surnames = {'Iwanski', 'Moscicki'};
    gestures = {'ZADUMA', 'ZDZIWIENIE'};
    NPCsLocation = {[-0.07 0.35 2.19], [7.97 0.59 5.24]};
    keepFiguresOpened = false;
    saveResultsToFiles = false;
end

% preallocation of data
gesturesData = cell(size(gestures,2), 1);

for person=1:size(names,2)
    for gesture=1:size(gestures,2)      
        [result, outData] = showResults(names{person}, surnames{person}, gestures{gesture}, attemptsPerGesture, NPCsLocation{gesture}+globalOffset, {NPCNamesPrefixes{gesture}});
        gesturesData{gesture, person} = outData;
        if saveResultsToFiles == true
            filename = strcat(savePath, gestures{gesture}, '_', names{person}, '_', surnames{person});
            saveas(result, filename, 'png');
        end
        if keepFiguresOpened == false
            close(result);
        else
            refresh(result);
        end
    end
end

if printStatisticAnalisys == true
    if clearConsoleBeforeAnalisys == true
        clc
    end        
    
    for gesture=1:size(gestures,2)
        fprintf('\n%s PARAMETERS:\n\n', gestures{gesture});
        parametersAmount = size(statisticEntityFormat, 1);
        for param=1:parametersAmount
            paramID = statisticEntityFormat{param}{1};
            paramRounder = statisticEntityFormat{param}{2};
            comparementLevels = statisticEntityFormat{param}{3};
            paramTitle = statisticEntityFormat{param}{4};
            inp = [];
            for person=1:size(names,2)
                if (param == 8 || param == 10)
                    samplesTrue = gesturesData{gesture,person}{param}(1);
                    samplesFalse = gesturesData{gesture,person}{param}(2) - samplesTrue;
                    if(samplesFalse > 0 && samplesTrue > 0)
                        samplesTArray = repmat(1, 1, samplesTrue);
                        samplesFArray = repmat(0, 1, samplesFalse);
                        inp = [ inp samplesTArray samplesFArray ];
                    else
                        if(samplesTrue <= 0)
                            samplesFArray = repmat(0, 1, samplesFalse);
                            inp = [ inp samplesFArray ];
                        else
                            samplesTArray = repmat(1, 1, samplesTrue);
                            inp = [ inp samplesTArray ];
                        end
                    end
                else
                    inp = [ inp gesturesData{gesture,person}{param}' ];
                end
            end
            inp = roundNumberTo(inp, paramRounder);
            printStatistics(inp, {comparementLevels}, {paramTitle}, maxShownResultsOfOccurenceBeforeHidingProcedure);
        end
    end
end

disp('All tasks finished!');
