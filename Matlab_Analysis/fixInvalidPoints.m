function [fixedX fixedY] = fixInvalidPoints(X, Y)
    fixedX = X;
    fixedY = Y;
% 
%     fixedX = fixedX(fixedX>0);
%     fixedY = fixedY(fixedX>0);
%     fixedX = fixedX(fixedX<1);
%     fixedY = fixedY(fixedX<1);
%     
%     fixedX = fixedX(fixedY>0);
%     fixedY = fixedY(fixedY>0);
%     fixedX = fixedX(fixedY<1);
%     fixedY = fixedY(fixedY<1);

    fixedX(fixedX>1) = 1;
    fixedX(fixedX<0) = 0;
    fixedY(fixedY>1) = 1;
    fixedY(fixedY<0) = 0;
    
end

