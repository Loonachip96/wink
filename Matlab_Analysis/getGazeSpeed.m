function [avg, maximum, minimum, stdDev] = getGazeSpeed(Xs, Ys, deltaTimes)
    listLength = size(Xs, 1);
    speeds = zeros(listLength-1, 1);
    
    for i=1:listLength-1
        speeds(i) = sqrt( (Xs(i)-Xs(i+1))^2 + (Ys(i)-Ys(i+1))^2 )/deltaTimes(i+1);
    end
    
    avg = mean(speeds);
    maximum = max(speeds);
    minimum = min(speeds);
    stdDev = std(speeds);
end

