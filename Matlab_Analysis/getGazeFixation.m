function fix = getGazeFixation(Xs, Ys)
    k = 4;
    allPoints = [Xs; Ys];  
    
%     centralMoment = mean( allPoints - mean(allPoints)).^k;    
%     standardDeviation = std(allPoints);    
%     fix = centralMoment / standardDeviation;
    
    fix = 1 - mean([std(Xs); std(Ys)]);

end

