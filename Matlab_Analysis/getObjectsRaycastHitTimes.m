function [objectNames, percentageTime, sampleAmounts] = getObjectsRaycastHitTimes(hitNames)
    objectNames = unique(hitNames, 'rows');
    objectsCount = size(objectNames,1);
    samplesAmount = size(hitNames, 1);
    
    sampleAmounts = zeros(objectsCount, 1);
    percentageTime = zeros(objectsCount, 1);
    
    for iUniqueObject=1:objectsCount
        sampleAmounts(iUniqueObject) = sum(sum(hitNames(:,:) == objectNames(iUniqueObject,:), 2) == size(hitNames, 2));
        percentageTime(iUniqueObject) = sampleAmounts(iUniqueObject)/samplesAmount*100;
    end
end

