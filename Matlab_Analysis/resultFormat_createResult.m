function [outputFigure outputData] = resultFormat_createResult(figureTitle, dataSets, NPCPosition, NPCNamesPrefixes)
    heatmapX = 1200;
    heatmapY = 1200;
    heatmapBrushRatio = 180;
    heatmapIntensivity = 0.10;

    idefLR = 1;
    idefFB = 2;
    irot = 3;
    iAbsDefLR = 4;
    iAbsDefFB = 5;
    iAbsRot = 6;
    iNPCGazingTime = 7;
    iNPCGazingSamples = 8;
    iNPCFacingTime = 9;
    iNPCFacingSamples = 10;
    iGazeUncertainty = 11;
    iAvgGazeSpeed = 12;
    iMaximumGazeSpeed = 13;
    iMinimumGazeSpeed = 14;
    iStdDevGazeSpeed = 15;
    iGazeFixation = 16;
    iHeadFixation = 17;
    outputData = cell(17, 1);
    outputData{iNPCGazingTime} = zeros(1,3);
    outputData{iNPCFacingTime} = zeros(1,3);
    outputData{iGazeUncertainty} = zeros(1,3);
    outputData{iAvgGazeSpeed} = zeros(1,3);
    outputData{iMaximumGazeSpeed} = zeros(1,3);
    outputData{iMinimumGazeSpeed} = zeros(1,3);
    outputData{iStdDevGazeSpeed} = zeros(1,3);
    outputData{iGazeFixation} = zeros(1,3);
    outputData{iHeadFixation} = zeros(1,3);
    
    headPositionColumn = 1;
    relativeHeadPositionColumn = 2;
    traceColumn = 3;
    heatmapColumn = 4;
    gazeTextColumn = 5;
    faceTextColumn = 6;
    parametersColumn = 7;
    columnsAmount = 7;
    xoffset = [-0.1 -0.1 -0.1 -0.1 -0.15 -0.1 -0.07];
    
    setsAmount = size(dataSets, 1);
    rowsAmount = setsAmount*3;
    
    outputFigure = figure('Name',figureTitle, 'NumberTitle', 'off', 'Position',get(0, 'Screensize'));

    disp(strcat('Creating:_', figureTitle));
    for iDataSet=1:setsAmount
        
        disp(strcat('Handling dataset no.', num2str(iDataSet)));
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            HRXs = dataset_getColumn(dataSets{iDataSet}, 'headRotationX');
            HRYs = dataset_getColumn(dataSets{iDataSet}, 'headRotationY');
            HRZs = dataset_getColumn(dataSets{iDataSet}, 'headRotationZ');
            HRWs = dataset_getColumn(dataSets{iDataSet}, 'headRotationW');
            HXs = dataset_getColumn(dataSets{iDataSet}, 'headX');
            HYs = dataset_getColumn(dataSets{iDataSet}, 'headY');
            HZs = dataset_getColumn(dataSets{iDataSet}, 'headZ');
            playerPosition = [HXs HYs HZs];
            deltaTimes = dataset_getColumn(dataSets{iDataSet}, 'deltaTime');
            [defLR defFB rot] = getRelativeHeadDeflection(HRXs, HRYs, HRZs, HRWs, playerPosition, NPCPosition); 
            plotXs = getTimeAxisFromDeltas(deltaTimes);
            outputData{idefLR} = defLR;
            outputData{idefFB} = defFB;
            outputData{irot} = rot;
            outputData{iAbsDefLR} = abs(defLR);
            outputData{iAbsDefFB} = abs(defFB);
            outputData{iAbsRot} = abs(rot);
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            gridIDs = getSubplotSpanNumbers(headPositionColumn, (iDataSet-1)*3+1, 1, 1, columnsAmount);
            calcs = subplot(rowsAmount, columnsAmount, gridIDs);
                plot(plotXs, defFB, 'c.');
            amplitude = max(abs(defFB));
                axis([0 plotXs(end) -amplitude amplitude]);
            title('Head deflection (forw(+) backw(-))');
                gridPos = get(calcs, 'Position');
                gridPos(1) = gridPos(1) + xoffset(headPositionColumn);
            set(calcs, 'Position', gridPos);
            %%%%%%%%%%%%%%
            
            gridIDs = getSubplotSpanNumbers(headPositionColumn, (iDataSet-1)*3+2, 1, 1, columnsAmount);
            calcs = subplot(rowsAmount, columnsAmount, gridIDs);
                plot(plotXs, defLR, 'm.');
            amplitude = max(abs(defLR));
                axis([0 plotXs(end) -amplitude amplitude]);
            title('Head deflection (left(-) right(+))');
                gridPos = get(calcs, 'Position');
                gridPos(1) = gridPos(1) + xoffset(headPositionColumn);
            set(calcs, 'Position', gridPos);
            %%%%%%%%%%%%%%
            
            gridIDs = getSubplotSpanNumbers(headPositionColumn, (iDataSet-1)*3+3, 1, 1, columnsAmount);
            calcs = subplot(rowsAmount, columnsAmount, gridIDs);
                plot(plotXs, rot, 'k.');
            amplitude = max(abs(rot));
                axis([0 plotXs(end) -amplitude amplitude]);
            title('Relative head rotation (clockw is negative)');
                gridPos = get(calcs, 'Position');
                gridPos(1) = gridPos(1) + xoffset(headPositionColumn);
            set(calcs, 'Position', gridPos);
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % getSubplotSpanNumbers(posX, posY, spanX, spanY, resolX)
            
            gridIDs = getSubplotSpanNumbers(relativeHeadPositionColumn, (iDataSet-1)*3+1, 1, 1, columnsAmount);
            calcs = subplot(rowsAmount, columnsAmount, gridIDs);
                plot(plotXs, abs(defFB), 'c.');
            amplitude = max(abs(defFB));
                axis([0 plotXs(end) 0 amplitude]);
            title('Absolute head deflection');
                gridPos = get(calcs, 'Position');
                gridPos(1) = gridPos(1) + xoffset(relativeHeadPositionColumn);
            set(calcs, 'Position', gridPos);
            %%%%%%%%%%%%%%
            
            gridIDs = getSubplotSpanNumbers(relativeHeadPositionColumn, (iDataSet-1)*3+2, 1, 1, columnsAmount);
            calcs = subplot(rowsAmount, columnsAmount, gridIDs);
                plot(plotXs, abs(defLR), 'm.');
            amplitude = max(abs(defLR));
                axis([0 plotXs(end) 0 amplitude]);
            title('Absolute head deflection');
                gridPos = get(calcs, 'Position');
                gridPos(1) = gridPos(1) + xoffset(relativeHeadPositionColumn);
            set(calcs, 'Position', gridPos);
            %%%%%%%%%%%%%%
            
            gridIDs = getSubplotSpanNumbers(relativeHeadPositionColumn, (iDataSet-1)*3+3, 1, 1, columnsAmount);
            calcs = subplot(rowsAmount, columnsAmount, gridIDs);
                plot(plotXs, abs(rot), 'k.');
            amplitude = max(abs(rot));
                axis([0 plotXs(end) 0 amplitude]);
            title('Absolute relative head rotation');
                gridPos = get(calcs, 'Position');
                gridPos(1) = gridPos(1) + xoffset(relativeHeadPositionColumn);
            set(calcs, 'Position', gridPos);
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            rawGazeXs = dataset_getColumn(dataSets{iDataSet}, 'gazeX');
            rawGazeYs = dataset_getColumn(dataSets{iDataSet}, 'gazeY');
            [gazeXs gazeYs] = fixInvalidPoints(rawGazeXs, rawGazeYs);
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
        
            disp('   Creating gaze trace plot');
            gridIDs = getSubplotSpanNumbers(traceColumn, (iDataSet-1)*3+1, 1, 3, columnsAmount);
            calcs = subplot(rowsAmount, columnsAmount, gridIDs);
                tracePlot(gazeXs, gazeYs);
                title('Gaze points and their trace');
                axis([0 1 0 1]);
                set(calcs,'YTickLabel',[]);
                set(calcs,'XTickLabel',[]);
                    gridPos = get(calcs, 'Position');
                    gridPos(1) = gridPos(1) + xoffset(traceColumn);
                set(calcs, 'Position', gridPos);
            %%%%%%%%%%%%%%
            
            disp('   Creating gaze heatmap');
            gridIDs = getSubplotSpanNumbers(heatmapColumn, (iDataSet-1)*3+1, 1, 3, columnsAmount);
            calcs = subplot(rowsAmount, columnsAmount, gridIDs);
                surf(getHeatmap(gazeXs, gazeYs, heatmapBrushRatio, heatmapIntensivity, heatmapX, heatmapY), 'EdgeColor', 'none');   
                title('Gaze heatmap');
                axis([0 heatmapX 0 heatmapY 0 inf]);
                set(calcs,'YTickLabel',[]);
                set(calcs,'XTickLabel',[]);
                view(270, 90)
                    gridPos = get(calcs, 'Position');
                    gridPos(1) = gridPos(1) + xoffset(heatmapColumn);
                set(calcs, 'Position', gridPos);
            %%%%%%%%%%%%%%
            
            disp('   Calculating gazing objects parameters');
            gridIDs = getSubplotSpanNumbers(gazeTextColumn, (iDataSet-1)*3+1, 1, 3, columnsAmount);
            calcs = subplot(rowsAmount, columnsAmount, gridIDs);
                lines = {};
                hitNames = dataset_getColumn(dataSets{iDataSet}, 'gazeRaycastHitObjectName');
                [objectNames, percentageTimes, sampleAmounts] = getObjectsRaycastHitTimes(hitNames);
                lines = format_RaycastHits(lines, objectNames, percentageTimes, 'Objects hit by gaze with percentage time:');
                format_PrintLineBuffer(lines);
                set(calcs, 'visible', 'off');
                    gridPos = get(calcs, 'Position');
                    gridPos(1) = gridPos(1) + xoffset(gazeTextColumn);
                set(calcs, 'Position', gridPos);
            iObj = findIndexOfFirstStringWithPrefix(objectNames, NPCNamesPrefixes);
            if(iObj > 0)
                outputData{iNPCGazingSamples} = [sampleAmounts(iObj) size(hitNames, 1)];
                outputData{iNPCGazingTime}(iDataSet) =  percentageTimes(iObj);
            else
                outputData{iNPCGazingSamples} = [0 size(hitNames, 1)];
                outputData{iNPCGazingTime}(iDataSet) =  0;
            end
            
            %%%%%%%%%%%%%%

            disp('   Calculating facing objects parameters');
            gridIDs = getSubplotSpanNumbers(faceTextColumn, (iDataSet-1)*3+1, 1, 3, columnsAmount);
            calcs = subplot(rowsAmount, columnsAmount, gridIDs);
                lines = {};
                hitNames = dataset_getColumn(dataSets{iDataSet}, 'faceRaycastHitObjectName');
                [objectNames, percentageTimes, sampleAmounts] = getObjectsRaycastHitTimes(hitNames);
                lines = format_RaycastHits(lines, objectNames, percentageTimes, 'Objects in front of face with percentage time:');
                format_PrintLineBuffer(lines);
                set(calcs, 'visible', 'off');
                    gridPos = get(calcs, 'Position');
                    gridPos(1) = gridPos(1) + xoffset(faceTextColumn);
                set(calcs, 'Position', gridPos);
            iObj = findIndexOfFirstStringWithPrefix(objectNames, NPCNamesPrefixes);
            if(iObj > 0)
                outputData{iNPCFacingSamples} = [ sampleAmounts(iObj) size(hitNames, 1) ];
                outputData{iNPCFacingTime}(iDataSet) =  percentageTimes(iObj);
            else
                outputData{iNPCFacingSamples} = [ 0 size(hitNames, 1) ];
                outputData{iNPCFacingTime}(iDataSet) =  0;
            end
            
            %%%%%%%%%%%%%%

            disp('   Calculating gaze and head parameters');
            gridIDs = getSubplotSpanNumbers(parametersColumn, (iDataSet-1)*3+1, 1, 3, columnsAmount);
            calcs = subplot(rowsAmount, columnsAmount, gridIDs);
                lines = {};
                uncertsX = dataset_getColumn(dataSets{iDataSet}, 'avgXGazingUncertainty');
                uncertsY = dataset_getColumn(dataSets{iDataSet}, 'avgYGazingUncertainty');
                gazeUncertainty = mean([ mean(uncertsX) mean(uncertsY)]);
                [avg, maximum, minimum, stdDev] = getGazeSpeed(gazeXs, gazeYs, deltaTimes);
                lines = format_GazingSpeed(lines, avg, maximum, minimum, stdDev, gazeUncertainty);
                gazeFixation = getGazeFixation(gazeXs, gazeYs);
                headFixation = getHeadFixation(HRXs, HRYs, HRZs, HRWs);
                lines = format_GazeFixation(lines, gazeFixation);
                lines = format_HeadFixation(lines, headFixation);
                format_PrintLineBuffer(lines);
                set(calcs, 'visible', 'off');
                    gridPos = get(calcs, 'Position');
                    gridPos(1) = gridPos(1) + xoffset(parametersColumn);
                set(calcs, 'Position', gridPos);
                outputData{iGazeUncertainty}(iDataSet) = gazeUncertainty;
                outputData{iAvgGazeSpeed}(iDataSet) = avg;
                outputData{iMaximumGazeSpeed}(iDataSet) = maximum;
                outputData{iMinimumGazeSpeed}(iDataSet) = minimum;
                outputData{iStdDevGazeSpeed}(iDataSet) = stdDev;
                outputData{iGazeFixation}(iDataSet) = gazeFixation;
                outputData{iHeadFixation}(iDataSet) = headFixation;
            %%%%%%%%%%%%%%
    end
    outputData{iNPCGazingTime} = outputData{iNPCGazingTime}';
    outputData{iNPCFacingTime} = outputData{iNPCFacingTime}';
    outputData{iGazeUncertainty} = outputData{iGazeUncertainty}';
    outputData{iAvgGazeSpeed} = outputData{iAvgGazeSpeed}';
    outputData{iMaximumGazeSpeed} = outputData{iMaximumGazeSpeed}';
    outputData{iMinimumGazeSpeed} = outputData{iMinimumGazeSpeed}';
    outputData{iStdDevGazeSpeed} = outputData{iStdDevGazeSpeed}';
    outputData{iGazeFixation} = outputData{iGazeFixation}';
    outputData{iHeadFixation} = outputData{iHeadFixation}';
    disp(strcat('Finished:_', figureTitle));
end

