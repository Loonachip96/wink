function [occurence, occurringValues, samplesLessThanOrEqualLevels, samplesGreaterOrEqualLevels] = occurenceVector(data, comparementLevels)
    dataRowsAmount = size(data, 1);
    
    samplesGreaterOrEqualLevels = cell(dataRowsAmount, 1);
    samplesLessThanOrEqualLevels = cell(dataRowsAmount, 1);
    occurringValues = cell(dataRowsAmount, 1);
    occurence = cell(dataRowsAmount, 1);
        
    for row = 1:dataRowsAmount
        samples = data(row, :);
        samplesGreaterOrEqualLevels{row} = sum( samples >= comparementLevels{row}(:), 2);
        samplesLessThanOrEqualLevels{row} = sum( samples <= comparementLevels{row}(:), 2);
        occurringValues{row} = unique(samples);
        occurence{row} = sum(samples(:) == unique(samples));
    end
end

